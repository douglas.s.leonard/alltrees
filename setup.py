import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

exec(open('gitalltrees/version.py').read())

setuptools.setup(
    name='git-alltrees',
    version=__version__,
    author='Doug Leonard',
    author_email='dleonard.dev@gmail.com',
    description='A subtree alternative with root-trees and merged remote histories',
    long_description_content_type="text/markdown",
    url='https://gitlab.com/douglas.s.leonard/git-alltrees',
    project_urls = {
        "Bug Tracker": "https://gitlab.com/douglas.s.leonard/git-alltrees"
    },
    license='MIT',
    packages=['gitalltrees'],
    package_data={'gitalltrees': ['py.typed']},
    install_requires=['qx @ https://gitlab.com/douglas.s.leonard/qx/-/archive/v1.1.0/qx-v1.1.0.tar#egg=qx-1.1.0'],
#    install_requires=[''],
    include_package_data=True,
    entry_points={
     'console_scripts': [
         'git-alltrees=gitalltrees:main',
     ],
    },
)
