#!/bin/bash
cwd=`pwd`
alltrees=git-alltrees
#alltrees=${cwd}/../alltrees/git-alltrees
function null(){
    :
}
#gitk=gitk
gitk=null  # don't display, assert only

assert(){
  assertion=$1
  if [ ! $assertion ]; then
    echo "Assertion Faile: " $assertion
    exit 1
  fi
}


declare -a repo_list=('tagtree_remote' 'testtag')
kill $(ps -ef | grep 'gitk' | grep -v 'grep' | awk {'print $2'})

#Git uses 1s time stamps.  $sleep keeps commits in order but slows down the script

sleep=echo
cwd=`pwd`

count=0
for repo in ${repo_list[*]};
do
  cd ${cwd}
  rm -rf "testrepos/${repo}"   # harded coded safety
  mkdir -p "testrepos/${repo}"
  cd "testrepos/${repo}"
  git init
  git checkout -b null
  git commit -m "initial null commit" --allow-empty
done

cd ../testtag # redundant
git checkout --orphan master

${alltrees} add tagtree tagtree/ ../tagtree_remote

git config pull.rebase false
mkdir tagtree

max=11
for i in `seq 1 $max`
do
  echo $i >>  tagtree/count    # add a file to root tree
  git add --all
  git commit -a -m "commit -$((max-$i))"
  $sleep 1  # allow git to understand the commit order
done
  
$alltrees push tagtree master:master

git tag -a testtag -m "testtag-conflict"
git checkout HEAD~1
git tag -a starttag1 -m "newtag"
git checkout HEAD~1
git tag starttag2 
git checkout HEAD~1
git tag  starttag3 
git checkout HEAD~1
git tag  starttag4 
git checkout master
${alltrees} push tagtree master:master --tags 
git checkout starttag4

git tag -d $(git tag -l)   # delete all tags.

git checkout HEAD~1
git tag  forcetag3   # this will displace starttag3 on forced push rename
git checkout HEAD~1
git tag  finaltag1   # this will be displaced by pushedtag1 on forced pull rename
git checkout HEAD~1
git tag  finaltag2   # this won't ever change
git checkout HEAD~1
git tag  finaltag5   # this one won't conflict
git checkout master
${alltrees} push tagtree --tags --tag-rename force:start --force -- master:master

#Create one more that will be a conflict on pull wihout renaming involved:
git checkout finaltag5
git checkout HEAD~1
git tag  testtag     

#and one more with no conflict
git checkout HEAD~1
git tag noconflict     


#$gitk --all &
#sleep 3

cd ../tagtree_remote
#$gitk --all &

cd ../testtag

function asserttags() {
    for i in `seq 0 $max`
    do
        readtag=`git describe --tags --exact-match "HEAD~${i}" 2>/dev/null`
        if [[ ${readtag} != ${tags[$i]} ]]; then
            echo Assertion for HEAD~${i} Failed:  "${readtag} == ${tags[$i]}"
            exit 1
        fi
    done
}

git checkout master
$alltrees pull tagtree master:master --tags --tag-rename start:final

#Assertions, 
declare -A tags
tags[4]="finaltag4"
tags[5]="finaltag3"
tags[6]="finaltag1"
tags[7]="finaltag2"
tags[8]="finaltag5"
tags[9]="testtag"
tags[10]="noconflict"

asserttags  # empty tags are also asserted
#$gitk --all &
#sleep 10 # need to sleep after backgrounding gitk

git checkout master
$alltrees pull tagtree master:master --tags --tag-rename start:final --force
$gitk --all &

#Assertions:
tags=()
tags[0]="testtag"
tags[1]="finaltag1"
tags[2]="finaltag2"
tags[4]="finaltag4"
tags[5]="finaltag3"
tags[8]="finaltag5"
tags[10]="noconflict"

asserttags

echo "Tag tests passed.  This does not test tags in complex subtree merges"
